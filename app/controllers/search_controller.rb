require_all 'app/services'

class SearchController < ApplicationController
  def show
  end

  def search_phone
    respond_to :js
    @items = collect_infos_for search_request[:text]
  end

  private

  def search_request
    params.require(:request).permit(:text)
  end

  def collect_infos_for(request)
    ParserMaster.subclasses.map { |parser| parser.parse(request) }.compact
  end
end
