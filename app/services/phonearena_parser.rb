class PhonearenaParser < ParserMaster
  class << self
    BASE_URL = 'http://www.phonearena.com'
    SEARCH_URL = 'http://www.phonearena.com/search?term='
    SPEC_NAMES = ['Dimensions', 'Materials'] # Added second attribute for example

    def parse(request)
      result_page = parse_page URI::encode(SEARCH_URL + request)
      return nil if check_results result_page

      compose_result(parse_page(first_рhone_url(result_page)))
    end

    def site_name
      'phonearena.com'
    end

    private

    def check_results(page)
      page.at_css('#phones > .s_no_result').present?
    end

    def model_name(page)
      page.at_css('#phone > h1 > span').text
    end

    def first_рhone_url(page)
      BASE_URL + page.at_css('.s_listing').at_css('h3 > a')['href']
    end

    def compose_result(page)
      attributes = SPEC_NAMES.reduce({}) do |result, spec|
        attribute = parse_specs(page, spec)
        attribute.present? ? result.merge(attribute) : result
      end
      PhoneInfo.new site_name, model_name(page), attributes
    end

    def parse_specs(page, spec_name)
      attribute = page.at("li.s_lv_1:contains('#{spec_name}')")&.at_css('ul')&.text
      return { "#{spec_name.downcase}": attribute } if attribute.present?
    end
  end
end
