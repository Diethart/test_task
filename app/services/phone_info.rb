# Simple container for phone info instead of OpenStruct

class PhoneInfo
  attr_reader :site_name, :model_name, :attributes

  def initialize(site_name, model_name, attributes = nil)
    @site_name, @model_name, @attributes = site_name, model_name, attributes
  end

  def read_attributes
    return 'Not found any data' if @attributes.blank?
    @attributes.reduce([]) do |memo, (key,val)|
      memo.push "#{key.capitalize}: #{val}"
    end.join('; ')
  end
end
