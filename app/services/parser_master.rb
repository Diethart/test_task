require 'nokogiri'

class ParserMaster
  class << self
    # Initialize Nokogiri page, pull out data and return it
    def parse(argument)
      raise NotImplementedError, "Subclasses must define `parse`."
    end

    # Get name of site
    def site_name
      raise NotImplementedError, "Subclasses must define `name`."
    end

    private

    # Check if there any result for request. Return 'Not found any model' if there is 0 results
    def check_results(page)
      raise NotImplementedError, "Subclasses must define `check_results`."
    end

    # Get name of founded phone model
    def model_name
      raise NotImplementedError, "Subclasses must define `model_name`."
    end

    # Default method for page parsing
    def parse_page(url)
      Nokogiri::HTML.parse(open(url))
    end

    # Get first item in search results
    def first_рhone_url(page)
      raise NotImplementedError, "Subclasses must define `first_рhone_url`."
    end

    # Compose founded information in one result: site name, founded model name and pulled out specs data. Uses PhoneInfo object as a container.
    def compose_result
      raise NotImplementedError, "Subclasses must define `compose_result`."
    end
  end
end
