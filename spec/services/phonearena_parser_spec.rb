require 'rails_helper'

RSpec.describe PhonearenaParser, type: :service do
  describe "parse 'test' request"  do
    let(:info) { PhonearenaParser.parse('test') }

    it 'should successfully create phone info object' do
      expect(info.model_name).to eq('Meizu MX6')
      expect(info.site_name).to eq('phonearena.com')
      expect(info.attributes[:dimensions]).to eq('6.05 x 2.96 x 0.29 inches  (153.6 x 75.2 x 7.25 mm)')
    end

    it 'should return nil when 0 results found' do
      expect(PhonearenaParser.parse('test_123')).to be nil
    end
  end
end
